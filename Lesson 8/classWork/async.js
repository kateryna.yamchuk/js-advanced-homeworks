/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    Company | Balance | Показать дату регистрации | Показать адресс |
    1. CompName 2000$ button button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/

document.addEventListener("DOMContentLoaded", function(event) {
  let table = document.getElementById("table");



  async function getCompanies(){
  	const ResponseList = await fetch("http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2")
  	const jsonResponse = await ResponseList.json();

  	let TABLE = jsonResponse.forEach ( (item) => {
  		let tr = document.createElement("tr");
  			table.appendChild(tr);

  		let td = document.createElement("td");
  			tr.appendChild(td);
  			td.innerHTML = item.company;

  		let td2 = document.createElement("td");
  			tr.appendChild(td2);
  			td2.innerHTML = item.balance;

  		let td3 = document.createElement("td");
  			tr.appendChild(td3);
  			let btn1 = document.createElement("button");
  				btn1.innerHTML = "show";
  				td3.appendChild(btn1);
  				btn1.addEventListener("click", ()=>{
  					td3.innerHTML = "";
  					td3.innerHTML = item.registered;
  				});

  		let td4 = document.createElement("td");
  			tr.appendChild(td4);
  			let btn2 = document.createElement("button");
  				btn2.innerHTML = "show";
  				td4.appendChild(btn2);
  				btn2.addEventListener("click", ()=>{
  					let address ="";
  					td4.innerHTML = "";
  					for (key in item.address) {
  						address = address + key + ":" + item.address[key] + " ";
  					}
  					td4.innerHTML = address;
  				})
  	})
  	return TABLE
}

	getCompanies();

	});






