/*
  Задача:

  1. При помощи fetch получить данные:
     http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2

  2. Полученый ответ преобразовать в json вызвав метод .json с объекта ответа
  3. Выбрать случайного человека и передать в следующий чейн промиса
  4. Сделать дополнительный запрос на получение списка друзей человека
     http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2
     И дальше передать обьект:
      {
        name: userName,
        ...
        friends: friendsData
      }

     Подсказка нужно вызвать дополнительный fecth из текущего чейна.
     Для того что бы передать результат выполнения доп. запроса
     в наш первый промис используйте констуркцию:

      .then(
        response1 => {
          return fetch(...)
            .then(
              response2 => {
                ...
                формируете обьект в котором будут данные человека с
                первого запроса, например его name response1.name
                и друзья которые пришли из доп. запроса
              }
            )
        }
      )

  5. Вывести результат на страничку

  + Бонус. Для конвертации обьекта response в json использовать одну
    функцию.

*/

  
  let url = "http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2";
  //
  var myHeaders = new Headers();
      myHeaders.append("Content-Type", "text/plain");
  //
  const ConvertToJSON = ( data ) => data.json();

    fetch( url, { method: 'POST', header: myHeaders} )
    .then( ConvertToJSON )
    .then( DataHandler )
    .then( (random_person) => {
      const userName = random_person.name;
      const favoriteFruit = random_person.favoriteFruit;
      const company = random_person.company;   
      return fetch ("http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2", {method:'POST',header: myHeaders} )
      .then( ConvertToJSON)
      .then( (json) => {
        let array_friends = [];
        json[0].friends.forEach ( (item, i) => {
          array_friends[i] = item.name;
        })
        const obj = {
          nameUser : userName,
          favoriteFruit: favoriteFruit,
          company: company,
          friends : array_friends
        }
        return obj

      })
    })
    .then( (obj) => {
      document.write("<br> User Name: " + obj.nameUser + "</br>");
      document.write("<br> Favourite Fruit: " + obj.favoriteFruit+ "</br>");
      document.write("<br> Company: " + obj.company+ "</br>");
      document.write("<br> Friends: " + obj.friends+ "</br>");
    })
    .catch( error => console.log('catch', error));


  function DataHandler( json ){
      //console.log( 'json', json );
      //json.map( item => console.log(item) )
      let rand = Math.floor(Math.random() * json.length);
      return json[rand];

  }




 /* fetch(url)
    .then(testFunc)
    .then(test2Func)
    .then( res => {
       fetch()

    })
    .then( func) */
