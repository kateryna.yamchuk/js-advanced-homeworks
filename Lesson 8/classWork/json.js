/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Arary

*/
window.addEventListener('load', () => {
	let btn = document.getElementById('btn');
	let btn2 = document.getElementById('btn2');

	let obj = {
			name:"",
			age:"",
			password:"",
		};

	btn.onclick = () => {
		obj.name = document.forms.form1.elements[0].value;
		obj.age = document.forms.form1.elements[1].value;
		obj.password = document.forms.form1.elements[2].value;
		console.log(JSON.stringify(obj));
	}

	btn2.onclick = () => {
		let json = document.forms.form2.elements[0].value;
		console.log(JSON.parse(json));
	}
});



