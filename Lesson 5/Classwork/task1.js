/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/

var Train = {
	name: "Intercity",
	speed: 150,
	pass_amount: 100,
	Go: function (){
		console.log("Поезд ",this.name," везет ",this.pass_amount ," со скоростью ",this.speed);
	},
	Stop: function(){
		this.speed = 0;
		console.log("Поезд", this.name, "остановился. Скорость", this.speed);
	},
	GetPass: function(){
		var x = 10;
		this.pass_amount += x;
		console.log(this.pass_amount);
	}

}
	Train.Go();
	Train.Stop();
	Train.GetPass();
	Train.GetPass();
	Train.GetPass();