/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.

    Например:
      encryptCesar('Word', 3);
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1('Sdwq', 3);
      decryptCesar1(...)
      ...
      decryptCesar5(...)
*/

const encryptCesar = (word,increment) => {
  let 
    massiv2 = [], 
    index, 
    new_word, 
    word_inc, 
    word_;
  const  
        LETTER_ya = 1103, // Я
        LETTER_YA = 1071, // я
        LETTER_a = 1072,  // а
        LETTER_A = 65;  // А
  for (let i =0; i < word.length; i++){
    word_ = word[i].charCodeAt();
    word_inc = word_ + increment;

    if (word_inc > LETTER_ya){
      index = (word_inc - LETTER_ya) + (LETTER_a - 1);
    }else if(word_inc > LETTER_YA && word_inc < LETTER_a){
      index = (word_inc - LETTER_YA) + (LETTER_A - 1);
    }else{
      index = word_inc;
    }
    massiv2[i] = String.fromCharCode(index);
  }
  new_word = massiv2.join("");
  return console.log("Слово:",word,"Шифр:",new_word);
}

const decryptCesar = (word,increment) => {
 let 
    massiv2 = [], 
    index, 
    new_word, 
    word_inc, 
    word_;
  const  
        LETTER_ya = 1103, // Я
        LETTER_YA = 1071, // я
        LETTER_a = 1072,  // а
        LETTER_A = 65;  // А
  
  for (let i =0; i < word.length; i++){
    word_ = word[i].charCodeAt();
    word_inc = word_ - increment;

    if (word_inc < LETTER_A){
      index = (LETTER_YA - word_inc) - (LETTER_YA + 1);
    }else if(word_inc < LETTER_a && word_inc > LETTER_YA){
      index = (LETTER_a - word_inc) - (LETTER_ya + 1);
    }else{
      index = word_inc;
    }
    massiv2[i] = String.fromCharCode(index);
  }
  new_word = massiv2.join("");
  return console.log("Шифр:",word,"Слово:",new_word);
}

const encryptCesar1 = encryptCesar.bind(null,"Машина",1);
encryptCesar1();

const encryptCesar2 = encryptCesar.bind(null,"Машина",2);
encryptCesar2();

const encryptCesar3 = encryptCesar.bind(null,"Машина",3);
encryptCesar3();

const encryptCesar4 = encryptCesar.bind(null,"Машина",4);
encryptCesar4();

const encryptCesar5 = encryptCesar.bind(null,"Машина",5);
encryptCesar5();

const decryptCesar1 = decryptCesar.bind(null,"Нбщйоб",1);
decryptCesar1();

const decryptCesar2 = decryptCesar.bind(null,"Овъкпв",2);
decryptCesar2();

const decryptCesar3 = decryptCesar.bind(null,"Пгылрг",3);
decryptCesar3();

const decryptCesar4 = decryptCesar.bind(null,"Рдьмсд",4);
decryptCesar4();

const decryptCesar5 = decryptCesar.bind(null,"Сеэнте",5);
decryptCesar5();