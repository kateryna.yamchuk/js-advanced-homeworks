/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>

*/
function Comment(name, text, avatarUrl) {
     this.name = name;
     this.text = text;
     this.avatarUrl = avatarUrl;
 }

const myComment1 = new Comment("Masha","Hello!","image1.jpg");
myComment1.likes = 0;

const myComment2 = new Comment("Sasha","))))))))))))))))))");
myComment2.likes = 0;

const myComment3 = new Comment("Liza","Bye(","image3.jpg");
myComment3.likes = 0;

const myComment4 = new Comment("Lida","^_^","image4.jpg");
myComment4.likes = 0;

const prototypeComment = {
  defaultAvatarUrl: 'image5.jpg',
  countLikes: function(){
    this.likes++;
  }
}
Object.setPrototypeOf( myComment1, prototypeComment );
Object.setPrototypeOf( myComment2, prototypeComment );
Object.setPrototypeOf( myComment3, prototypeComment );
Object.setPrototypeOf( myComment4, prototypeComment );


const CommentsArray = [myComment1, myComment2, myComment3, myComment4];

const Constructor = (array) => {
  let div = document.getElementById("CommentsFeed");
  let ul = document.createElement("ul");
  div.append(ul);
  for (let i = 0; i<array.length; i++){
    let li = document.createElement("li");
       ul.append(li);

    let div_main = document.createElement("div");
      div_main.setAttribute("id","div_main");
      li.append(div_main);

    let div1 = document.createElement("div");
       div_main.append(div1);
       div1.style.paddingRight = "10px";
       div1.style.paddingLeft = "5px";

    let div2 = document.createElement("div");
       div_main.append(div2);
       div2.style.width = "100px";

    let div3 = document.createElement("div");
       div_main.append(div3);
        div3.style.width = "200px";

    let div4 = document.createElement("div");
       div_main.append(div4);
        div4.style.width = "80px";

    let div5 = document.createElement("div");
        div5.style.width = "80px";
       div_main.append(div5);

    let img = document.createElement("img");
          div1.append(img);
          img.src = array[i].avatarUrl || array[i].defaultAvatarUrl ;
          img.setAttribute("width",60);
          img.setAttribute("height",60);

        let span_name = document.createElement("span");
          div2.append(span_name);
          span_name.innerText = array[i].name;

        let span_comment = document.createElement("span");
          div3.append(span_comment);
          span_comment.innerText = array[i].text;

        let btn = document.createElement("button");
          div4.append(btn);
          btn.innerText = "Лайкнуть!";  
          

        let span_likes = document.createElement("span");
          div5.append(span_likes);
          span_likes.innerText = "likes: "+array[i].likes;

        btn.addEventListener("click", function(){
          array[i].countLikes();
          span_likes.innerText = "likes: "+array[i].likes;
        })
  }
  

}
Constructor(CommentsArray);




